# Devoir 2 SR03 : application d'un salon de discussion (chats)
## Membres:
- CARREZ Emilien
- CHÁVEZ HERREJÓN Andrea

## Tutoriel installation du git en local 
##### Pour télécharger:
Tapez les commandes successives dans le repertoire de votre choix:
1. SSH
```bash
git clone git@gitlab.utc.fr:achavezh/devoir2_sr03.git
```
```bash
git clone https://gitlab.utc.fr/achavezh/devoir2_sr03.git
```
```bash
cd devoir2_sr03 											#on se place dans le répertoire git
```

##### Pour télécharger les mises à jour (il faut le faire avant toute modification)
```bash 
git pull
```
##### Pour mettre à jour ses modifications
```bash
git commit -a -m "message associé au commit"        # commit = sauvegarder en local (l'option -a veut dire all)
git push 											# pour uploader tous le code modifié (commited)
```

##### Pour ajouter tous les nouveaux fichiers créés localement au projet
```bash
git add * 										    # à faire avant le nouveau commit
```
## Tutoriel d'utilisation
##### Pour importer la base de données sur MySQL Workbench

Sur Workbench : Utilisez votre utilisateur et votre mot de passe et allez vers l'onglet "Administration", ensuite vous devez suivre les onglets, sections ou boutons suivantes :
1. Data Import/Restore
2. Import options : Aconseillé de choisir "Import from Self-Contained File" et après vous devez chercher et choisir le fichier téléchargé "bdd.sql"
3. Default Schema to be imported : Cliquez sur "New" pour creer une nouvelle base de donnes sinon chercher le nom de base de données que vous avez déjà choisi (C'est recommendable d'appeler la base de données "sr03")
4. Start Import

##### Pour importer le projet sur Eclipse
1. Vous devez suivre les onglets ou boutons suivantes :
```bash
File > Import > Existing Projects into Workspace > Next
```
Après on se place dans le repertoire du projet et vous cliquez sur "Finish"

##### Pour compiler et run le projet

1. Avant de compiler le projet, vous devez vérifier que les informations qui contient le fichier "fichierProp.properties" a les bons paramètres sinon vous devriez les changer. Le fichier est situé dans :

```bash
Devoir2 > Java Resources > Modele > fichierProp.properties
```
Vous pouvez bien sur changer l'utilisateur, mot de passe, etc. 

2. Après vous compilez en cliquant run as "Run on Server" dans le fichier "Connexion.jsp", situé dans : 
```bash
Devoir2 > WebContent > Vue > Connexion.jsp 
```
