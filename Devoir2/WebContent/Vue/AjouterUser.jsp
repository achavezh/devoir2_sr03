<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<!-- formulaire de création d'un utilisateur -->
<head>
<meta charset="ISO-8859-1">
<title>Ajouter User</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/design/login.css" />
</head>
<body>
<div class = "wrapperadd">
<div class = "containeradd">
<h2>Nouvel utilisateur</h2>
<form action="/Devoir2/GestionUser?create" method="post">
<h3> Prnom </h3>
<%
if(request.getAttribute("alreadyExist")==null) // envoyé par la servlet et permet de re remplir certain champs si la création est impossible
	out.println("<input type=\"text\" id=\"frname\" name=\"firstName\" required autocomplete=\"off\"/><br/>");
else{
	out.println("<input type=\"text\" id=\"frname\" name=\"firstName\" required autocomplete=\"off\" value=\""+request.getParameter("firstName")+"\" /><br/>");
}
%>
<h3> Nom de famille </h3>
<%
if(request.getAttribute("alreadyExist")==null)
	out.println("<input type=\"text\" id=\"faname\" name=\"famillyName\" required autocomplete=\"off\"/><br/>");
else
	out.println("<input type=\"text\" id=\"faname\" name=\"famillyName\" required autocomplete=\"off\" value=\""+request.getParameter("famillyName")+"\" /><br/>");
%> 
<h3> Email </h3> 
<input type="email" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required autocomplete="off"/> <br>
<h3> Mot de passe </h3> 
<%
if(request.getAttribute("alreadyExist")==null)
	out.println("<input type=\"password\" id=\"psw\" name=\"password\" required autocomplete=\"off\"/><br/>");
else
	out.println("<input type=\"password\" id=\"psw\" name=\"password\" required autocomplete=\"off\" value=\""+request.getParameter("password")+"\" /><br/>");
%>
<h3> Homme
<%
if(request.getAttribute("alreadyExist")==null || request.getParameter("gender").equals("male")){
	out.println("<input type=\"radio\" id=\"male\" name=\"gender\" value=\"male\" checked/><br/>");
}
else{
	out.println("<input type=\"radio\" id=\"male\" name=\"gender\" value=\"male\"/><br/>");
}
%>
Femme 
<%
if(request.getAttribute("alreadyExist")==null || !request.getParameter("gender").equals("female"))
	out.println("<input type=\"radio\" id=\"female\" name=\"gender\" value=\"female\"/><br/>");
else
	out.println("<input type=\"radio\" id=\"female\" name=\"gender\" value=\"female\" checked/><br/>");
%>
</h3>
<input type="submit" value="S'enregistrer"><br/>
</form>
<%
if(request.getAttribute("alreadyExist")!=null) // message d'erreur
	out.println("<p class=\"errmsg\">L'utilisateur existe dj, essayez une autre adresse mail</p>");
%>
</div>
</div>
</body>
</html>