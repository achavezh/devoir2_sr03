window.addEventListener("load", function(event) {
	/* on récupère l'id du chat, le login client et on créé une nouvelle webSocket */
	let id_chat = document.getElementById("id_chat");
	let login = document.getElementById("login");

	let ws = new WebSocket("ws://" + document.location.hostname + ":"
			+ document.location.port + "/Devoir2/ChatEndPoint/" + id_chat.value
			+ "/" + login.value);

	const messages = document.querySelector('#messages') /* div où l'on va placer les messages */ 

	let input = document.getElementById("txtMessage"); /* champ de saisie du message */
	txtMessage.focus();

	ws.addEventListener("open", function(evt) { /* on ajoute un évènement à la connexion de l'utilisateur au chat */
		console.log("Connection established");
	});

	ws.addEventListener("message", function(evt) { /* on ajoute un évènement à la réception d'un message par la webSocket côté cliente */
		let message = evt.data;
		const newMsg = document.createElement('div');
		var d = new Date();
		var pseudo = "";
		var msg = "";
		var i = 0;
		var extrem = "";

		/*on parse le message de la forme "True/false pseudo message" avec True/false qui indique si le message est un message de connexio/déconnexion ou non */
		while (message[i] != " ") {
			extrem = extrem + message[i];
			i++;
		}
		i++;

		while (message[i] != " ") {
			pseudo = pseudo + message[i];
			i++;
		}

		for (j = i; j < message.length; j++) {
			msg = msg + message[j];
		}
		/*Ici on place les messages dans une div: 3 div existent, la première est une div  pour un message de l'utilisateur, la deuxième pour un message d'un autre utilisateur, et la troisième pour une connexion/déconnexion */
		
		if (pseudo.localeCompare(login.value) == 0 /*message de l'utilisateur, on construit la div avec le message et le pseudo */ 
				&& extrem == "False") {
			newMsg.innerHTML = "<div class='text' style='width: 90%; float:right'>"
					+ "<b style='color: #009ABA'>" + pseudo + "</b><br/>"
					+ "<div style='float:left'><br/>" + msg
					+ "</div><br/><div style='float:right'><br/>"
					+ d.getHours() + ":" + d.getMinutes() + ":"
					+ d.getSeconds() + "</div><br/></div>";
		} else if (extrem == "False") { /*message d'un autre utilisateur, on construit la div avec le message et le pseudo */
			newMsg.innerHTML = "<div class='text-dark'style='width: 90%; float:left' >"
					+ "<b style='color: #F2BA1A'>" + pseudo + "</b><br/>"
					+ "<div style='float:left'><br/>" + msg
					+ "</div><br/><div style='float:right'><br/>" + d.getHours()
					+ ":" + d.getMinutes() + ":" + d.getSeconds()
					+ "</div><br/></div>";
		} else { /*message de connexion/déconnexion */
			console.log(extrem);
			newMsg.innerHTML = "<div class='text-dark'>"
					+ "<b style='color: black'>" + pseudo + "</b>" + msg
					+ "<br/><div style='float:left'><br/>" + d.getHours() + ":"
					+ d.getMinutes() + ":" + d.getSeconds()
					+ "</div><br/></div>";
		}

		messages.appendChild(newMsg); /* on rajoute la div message à la div principale*/
		console.log("Receive new message: " + message);

	});

	ws.addEventListener("close", function(evt) { /* on ajoute un évènement à la déconnexion de l'utilisateur au chat */
		console.log("Connection closed");
	});

	let btnSend = document.getElementById("btnSend");
	btnSend.addEventListener("click", function(clickEvent) { /*on ajoute une action lors du click sur le bouton envoie*/
		console.log(txtMessage.value);
		if (txtMessage.value.replaceAll(" ", "") != "") /*Si le message n'est pas vide*/
			ws.send(txtMessage.value); /* on envoie le message*/
		txtMessage.value = "";
		txtMessage.focus();
	});

	let btnClose = document.getElementById("btnClose");
	btnClose.addEventListener("click", function(clickEvent) { /*on ajoute une action lors du click sur le bouton déconnexion*/
		ws.close(); /* on ferme la webSocket côté cliente */

	});

});