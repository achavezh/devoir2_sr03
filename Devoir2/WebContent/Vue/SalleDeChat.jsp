<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="Modele.*"%>
<%@ page import="Controleur.*"%>
<%@ page import="java.lang.*"%>
<% 
//on check si la session n'est pas expirée ou incohérente
if (session.getAttribute("login") == null || session.getAttribute("login").equals("")){
	 response.sendRedirect("Connexion.jsp");
}else{
%>
<!DOCTYPE html>
<html>
<!-- salle de chat -->
<head>
<meta charset="ISO-8859-1">
<title>Salle de Chat</title>
<!-- script qui contient une fonction qui se lance toutes les 1000 ms pour mettre à jours la liste des users connected, et check que le chat n'est pas supprimé, expiré ou que l'utilisateur est encore invité dans le chat -->
<script type="text/javascript">
	 var id="<%=request.getParameter("id_chat")%>"; /* id du chat */
	 var date="<%=request.getParameter("date")%>";
	 var duree="<%=request.getParameter("duree")%>";
	 var horaire="<%=request.getParameter("horaire")%>";
	var stop = "false"; /*variable partagée entre ce JPS et le JSP UserConnected.jsp, détecte lorsque l'utilisateur doit être déconnecté de force*/
	/*on passe un ensemble d'argument au JSP que l'on appelle */
	var auto = setInterval(function() {
		$('#right').load(
				'UsersConnected.jsp?id_chat=' + id + '&date=' + date
						+ '&duree=' + duree + '&horaire=' + horaire).fadeIn(
				"slow");
		if (stop != "false") { /* cette valeur de stop est modifiée par UserConnected.jsp */
			clearInterval(auto); /* on arrête le lancement de la fonction toutes les 1000 ms*/
			window.alert(stop + "\n La fentre de chat va tre fermer."); /* on lance un pop up portant le message d'arrêt, par exemple expiration du chat*/
			document.getElementById('btnClose').click();/*on ferme le chat et la webSocket proprement avec l'appuie automatique sur le bouton btnClose qui a une fonction de fermeture associée dans le fichier client.js*/

		}

	}, 1000);
</script>
<script type="text/javascript" src="js/jquery.js"></script>
<!-- on lance le script de webSocket côté client -->
<script type="text/javascript" src="js/client.js"></script> 
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.min.js"></script>
<link rel="stylesheet"
	href="../design/chatRoom.css" />
</head>
<body onunload="document.getElementById('btnClose').click()"> <!-- à la fermeture de la fênetre on veut fermer proprement le chat ainsi que la webSocket -->
	<h2><%=request.getParameter("titre_chat")%></h2>
	<div class="left">
		<input type="hidden" id="id_chat"
			value='<%=request.getParameter("id_chat")%>' /> <input type="hidden"
			id="login" value=<%=session.getAttribute("login").toString()%> /> <input
			type="hidden" id="date" value=<%=request.getParameter("date")%> /> <input
			type="hidden" id="horaire" value=<%=request.getParameter("horaire")%> />
		<input type="hidden" id="duree"
			value=<%=request.getParameter("duree")%> />

		<div id='messages'></div> <!-- div portant les messages envoyés entre les utilisateurs -->
		<input id="txtMessage" type="text" 
				onkeydown="if (event.keyCode==13) document.getElementById('btnSend').click()" /> <!-- champ de saisi du message utilisateur -->
		<button id="btnSend">Envoyer</button>
		<button id="btnClose" onclick="window.close()">Dconnexion</button>

	</div>
	<div class="right">
		<div class="containerr" id="right"></div>
	</div>
	<br />
	<br />
</body>
</html>
<%}%>