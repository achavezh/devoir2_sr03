<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.Date"%>
<% 
if (session.getAttribute("login") == null || session.getAttribute("login").equals("")){
	 response.sendRedirect("Connexion.jsp");
}else{
%>
<!DOCTYPE html>
<html>
<!-- formulaire de création d'un chat -->
<head>
<meta charset="ISO-8859-1">
<title>Crer chats</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/design/navigation.css" />
</head>
<body>
<nav>
<!-- menu de navigation -->
<ul>
<li><a href='/Devoir2/Vue/Accueil.jsp'>Accueil</a></li>
<li class="deroulant"><a href='/Devoir2/Vue/Chats.jsp?actual&history'> Propritaire </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Chats.jsp?actual'> Mes Chats </a></li>
		<li><a href='/Devoir2/Vue/Chats.jsp?history'> Historique </a></li>
	</ul>
</li>
<li class="deroulant"><a href='/Devoir2/Vue/Invitations.jsp?actual&history'> Utilisateur </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Invitations.jsp?actual'> Mes Invitations </a></li>
		<li><a href='/Devoir2/Vue/Invitations.jsp?history'> Historique </a></li>
	</ul>
</li>
<li><a href='/Devoir2/Vue/CreerChat.jsp'>Crer un chat</a></li>
<li class="deroulant"><a href='#'> Paramtres </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/ChangeUser.jsp?firstUse'> Informations utilisateur </a></li>
	</ul>
</li>
<li>
<a href='/Devoir2/Deconnexion'>Dconnexion</a>
</li>
</ul>
</nav>
<div class = "wrapper">
<div class = "container">
<h1>Nouveau chat</h1><hr><br/>
<form action="/Devoir2/CreerChat" method="post">
<h3>Titre <br/><br/>
<%
if(request.getParameter("titre")!=null) /*dans le cas du formulaire déjà renvoyé par la servlet de gestion de création d'un chat car celui-ci a été créé dans le passé */
	out.println("<input type=\"text\" id=\"titre\" name=\"titre\" maxlength=\"100\" autocomplete=\"off\" required value=\""+request.getParameter("titre")+"\" /><br/>");
else
	out.println("<input type=\"text\" id=\"titre\" name=\"titre\" maxlength=\"100\" required autocomplete=\"off\"/><br/>");
%>
<br/>Description <br/><br/>
<%
if(request.getParameter("description")!=null)/*dans le cas du formulaire déjà renvoyé par la servlet de gestion de création d'un chat car celui-ci a été créé dans le passé */
	out.println("<input type=\"text\" id=\"description\" name=\"description\" maxlength=\"400\" autocomplete=\"off\" required value=\""+request.getParameter("description")+"\" /><br/>");
else
	out.println("<input type=\"text\" id=\"description\" name=\"description\" maxlength=\"400\" autocomplete=\"off\"/><br/>");
%>
<br/>Date<br/><br/>
<input type="date" id="date" name="date" required autocomplete="off" /> <br>
<br/>Horaire <br/><br/>
<input type="time" id="horaire" name="horaire" required autocomplete="off"/><br/>
<br/>Duree<br/><br/>
<%
if(request.getParameter("duree")!=null)/*dans le cas du formulaire déjà renvoyé par la servlet de gestion de création d'un chat car celui-ci a été créé dans le passé */
	out.println("<input type=\"time\" id=\"duree\" name=\"duree\" value="+request.getParameter("duree")+" required autocomplete=\"off\"/><br/>");
else
	out.println("<input type=\"time\" id=\"duree\" name=\"duree\" required autocomplete=\"off\"/><br/>");
%>
</h3> 
<input type="submit" class="button" value="Crr chat"><br/>
</form>
<%
if(request.getAttribute("pastChosen")!=null){ // si le chat a été défini dans le passé = erreur
%>
<p class=errmsg> Veuillez choisir une date ultrieure  la date actuelle </p>
<%
}
else
	if(request.getParameter("created")!=null){// si la création du chat a fonctionné
%>
<p class=msg> Les informations du chat ont bien ete enregistres </p>
<%
}
%>
</div>
</div>
<footer>
  <p>Authors: Andrea Chavez et Emilien Carrez</p>
</footer>
</body>
</html>
<%}%>