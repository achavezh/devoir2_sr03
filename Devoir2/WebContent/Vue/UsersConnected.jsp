<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"
	import="Controleur.ChatEndPoint, java.util.List, Modele.*, javax.servlet.*, java.util.Calendar,java.util.GregorianCalendar,java.util.Date,java.util.StringTokenizer"%>

<%
/* on met à jour la liste des utilisateurs connectés */
List<String> c = ChatEndPoint.usersConnected(request.getParameter("id_chat")); /* on cherche la liste des utilisateurs connectés */
out.println("<h3>Users Connected</h3>");
for (String u : c) {
	out.println("<br/><li>" + u + "</li><br/><hr>");
}
out.println("<br/>");

/*on regarde si le chat existe toujours*/
if (Chat.findByID(ConfigConnection.getUniqueInstance(), Integer.parseInt(request.getParameter("id_chat"))) == null) {
	System.out.println("chat enlev");
	/*si il n'existe plus on modifie la variable stop partagé avec le fichier SalleDeChat pour qu'un pop up apparaisse et que le chat du client se ferme, le pop up portera le message de stop */
	%>
	<script type="text/javascript">
	stop="Le chat a t supprim et n'est donc plus actif."
	</script>
	<%
}
/* on veut savoir si l'utilisateur n'a pas été désinvité du chat*/
String cli = (String) session.getAttribute("login");
int id_cli = User.getIdUser(ConfigConnection.getUniqueInstance(), cli);
int id_chat = Integer.parseInt(request.getParameter("id_chat"));
Chat chat= Chat.findByID(ConfigConnection.getUniqueInstance(), id_chat);
if(chat!=null){ // si le chat existe encore
	int id_owner=chat.getOwner(); /* on récupère le propriétaire du chat car celui-ci ne peut pas se virer lui même et n'est pas dans la table correspondance qui recense seulement les invités*/
	if (id_cli!=id_owner && CorrespClientChat.findByIDS(ConfigConnection.getUniqueInstance(), id_cli, id_chat) == null) { /*l'utilisateur n'est pas le propriétaire et il n'est plus dans la liste des invités*/
		System.out.println("utilisateur dsinvit");
		/*si l'utilisateur a été désinvité on modifie la variable stop partagé avec le fichier SalleDeChat pour qu'un pop up apparaisse et que le chat du client se ferme */
		%>
		<script type="text/javascript">
		stop="Vous n'tes dsormais plus utilisateur de ce chat."
		</script>
		<%
	}
}

/* on récupère la date/horaire/durée que l'on injecte dans des objets Calendar pour ensuite les comparer, on veut savoir si le chat est expiré*/
String date = request.getParameter("date");
StringTokenizer ST = new StringTokenizer(date, "-");
String year = ST.nextToken();
String month = ST.nextToken();
String day = ST.nextToken();
String horaire = request.getParameter("horaire");
String duree = request.getParameter("duree");
ST = new StringTokenizer(horaire, ":");
String hourH = ST.nextToken();
String minutesH = ST.nextToken();
ST = new StringTokenizer(duree, ":");
String hourD = ST.nextToken();
String minutesD = ST.nextToken();
Calendar cal=GregorianCalendar.getInstance();
cal.set(Integer.parseInt(year), Integer.parseInt(month)-1, Integer.parseInt(day), Integer.parseInt(hourH)+Integer.parseInt(hourD), Integer.parseInt(minutesH)+Integer.parseInt(minutesD),0);
Date dateSup=cal.getTime();
/*on regarde que le chat ne soit pas expiré (date de fin est avant la date actuelle)*/
if(dateSup.before(GregorianCalendar.getInstance().getTime()) || dateSup.equals(GregorianCalendar.getInstance().getTime())){
	System.out.println("Fin dure");
/*si il est expiré on modifie la variable stop partagé avec le fichier SalleDeChat pour qu'un pop up apparaisse et que le chat du client se ferme */
	%>
	<script type="text/javascript">
	stop="Le chat a atteint sa dure de fonctionnement."
	</script>
	<%
}

%>