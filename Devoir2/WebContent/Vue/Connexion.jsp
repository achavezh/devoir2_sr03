<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<!-- formulaire de connexion -->
<head>
<meta charset="ISO-8859-1">
<title>Connexion</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/design/login.css" />
</head>
<body>
<div class = "wrapperlogin">
<div class = "containerlogin">
<h2>Log-in</h2>
<form action="/Devoir2/Connexion" method="post">
<h3> Email </h3>
<%//dans le cas où la création de l'utilisateur a fonctionné et qu'il est redirigé vers la page de connexion pour se connecter
if(request.getAttribute("tryRegistered")!=null){%>
	<input type="email" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required autocomplete="off" 
	value="<%=request.getParameter("email")%>"/> <br/><br/>
<%}else{%>
	<input type="email" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
	required autocomplete="off" /> <br/><br/>
<%
}
%>
<h3> Mot de passe </h3>
<input type="password" id="psw" name="password" autocomplete="off" required/><br/><br/>
<input type="submit" value="Login"></input>
</form><br/><br/>
<a href="/Devoir2/Vue/AjouterUser.jsp">Crer un nouveau compte</a>
<% 
if(request.getAttribute("badValue")!=null) // on a essayé de se connecter avec un utilisateur non existant
	out.println("<p class=\"errmsg\">Votre login/mot de passe est incorrect</p>");
%>
</div>
</div>
</body>
</html>