<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ page import="Modele.*"%>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.sql.*" %>
<% //on vérifie si la session est valide ou existante
if (session.getAttribute("login") == null || session.getAttribute("login").equals("")){
	 response.sendRedirect("Connexion.jsp");
}else{
%>
<html>
<!-- JSP affichant les invitations et l'historique des chats invités de l'utilisateur -->
<head>
<meta charset="ISO-8859-1">
<title>Invitations</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/design/navigation.css" />
</head>
<body>
<nav>
<!-- menu de navigation -->
<ul>
<li><a href='/Devoir2/Vue/Accueil.jsp'>Accueil</a></li>
<li class="deroulant"><a href='/Devoir2/Vue/Chats.jsp?actual&history'> Propritaire </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Chats.jsp?actual'> Mes Chats </a></li>
		<li><a href='/Devoir2/Vue/Chats.jsp?history'> Historique </a></li>
	</ul>
</li>
<li class="deroulant"><a href='/Devoir2/Vue/Invitations.jsp?actual&history'> Utilisateur </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Invitations.jsp?actual'> Mes Invitations </a></li>
		<li><a href='/Devoir2/Vue/Invitations.jsp?history'> Historique </a></li>
	</ul>
</li>
<li><a href='/Devoir2/Vue/CreerChat.jsp'>Crer un chat</a></li>
<li class="deroulant"><a href='#'> Paramtres </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/ChangeUser.jsp?firstUse'> Informations utilisateur </a></li>
	</ul>
</li>
<li>
<a href='/Devoir2/Deconnexion'>Dconnexion</a>
</li>
</ul>
</nav>
<div class = "wrapper">
<div class = "container">
<%
if(request.getParameter("actual")!=null){ // on veut afficher les invitations de l'utilisateur (=chat invité actuel)
%>
<h1>Vos Invitations</h1><hr><br/><br/>
<table border ="1">
<tr>
    <th>#</th>
    <th>Titre</th>
    <th>Description</th>
    <th>Date</th>
    <th>Horaire</th>
    <th>Duree</th>
    <th></th>
</tr>
<%
String owner = (String) session.getAttribute("login");
int id = User.getIdUser(ConfigConnection.getUniqueInstance(), owner);
HashMap<Integer, Chat> chatsInvited = Chat.findByIDUser(id,false,true); /* on récupère les invitations de l'utilisateur (=chat ou l'utilsateur est  invité mais n'a pas encore donné sa réponse ) */
for(Map.Entry<Integer, Chat> co : chatsInvited.entrySet()){
%>
<tr>
	<td><%=co.getKey()%></td>
	<td><%=co.getValue().getTitre()%></td>
	<td><%=co.getValue().getDescription()%></td>
	<td><%=co.getValue().getDate()%></td>
	<td><%=co.getValue().getHoraire()%></td>
	<td><%=co.getValue().getDuree()%></td>
	<td><!-- on ajoute deux boutons pour accepter/refuser l'invitation -->
		<form action="/Devoir2/GestionInvit" method="post">
		<input type="hidden" name="id_chat" value=<%=co.getKey()%>>
		<input type="hidden" name="id_cli" value=<%=id%>>
		<input type="hidden" name="action" value="accepter">
		<input type="submit" value="Accepter"></input></form>
	    <form action="/Devoir2/GestionInvit" method="post">
		<input type="hidden" name="id_chat" value=<%=co.getKey()%>>
		<input type="hidden" name="id_cli" value=<%=id%>>
		<input type="hidden" name="action" value="refuser">
		<input type="submit" value="Refuser"></form>
	</td>
</tr>
<%
	}
}
%>
</table>
<%
if(request.getParameter("history")!=null){ /* on veut afficher les chats invités expirés de l'utilisateur (=chat où l'utilisateur a été invité et a répondu en acceptant l'invitation mais qui est expiré) */
%>
<h1>Historique de vos Invitation</h1><hr><br/><br/>
<table border ="1">
<tr>
    <th>#</th>
    <th>Titre</th>
    <th>Description</th>
    <th>Date</th>
    <th>Horaire</th>
    <th>Duree</th>
</tr>
<%
String owner = (String) session.getAttribute("login");
int id = User.getIdUser(ConfigConnection.getUniqueInstance(), owner);
HashMap<Integer, Chat> chatsInvited = Chat.findByIDUser(id,true,false); // on récupère ces chats en question
for(Map.Entry<Integer, Chat> co : chatsInvited.entrySet()){
%>
<tr>
	<td><%=co.getKey()%></td>
	<td><%=co.getValue().getTitre()%></td>
	<td><%=co.getValue().getDescription()%></td>
	<td><%=co.getValue().getDate()%></td>
	<td><%=co.getValue().getHoraire()%></td>
	<td><%=co.getValue().getDuree()%></td>
</tr>
<%
	}
}
%>
</table>
</div>
</div>
<footer>
  <p>Authors: Andrea Chavez et Emilien Carrez</p>
</footer>
</body>
</html>
<%}%>