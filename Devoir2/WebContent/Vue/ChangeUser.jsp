<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="Modele.*" %>
<% 
//on teste si la session existe et est cohérente
if (session.getAttribute("login") == null || session.getAttribute("login").equals("")){
	 response.sendRedirect("Connexion.jsp");
}else{
%>
<!DOCTYPE html>
<html>
<!-- formulaire de modifications des informations utilisateurs -->
<head>
<meta charset="ISO-8859-1">
<title>Change User</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/design/navigation.css" />
</head>
<body>
<nav>
<!-- barre de navigation -->
<ul>
<li><a href='/Devoir2/Vue/Accueil.jsp'>Accueil</a></li>
<li class="deroulant"><a href='/Devoir2/Vue/Chats.jsp?actual&history'> Propritaire </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Chats.jsp?actual'> Mes Chats </a></li>
		<li><a href='/Devoir2/Vue/Chats.jsp?history'> Historique </a></li>
	</ul>
</li>
<li class="deroulant"><a href='/Devoir2/Vue/Invitations.jsp?actual&history'> Utilisateur </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Invitations.jsp?actual'> Mes Invitations </a></li>
		<li><a href='/Devoir2/Vue/Invitations.jsp?history'> Historique </a></li>
	</ul>
</li>
<li><a href='/Devoir2/Vue/CreerChat.jsp'>Crer un chat</a></li>
<li class="deroulant"><a href='#'> Paramtres </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/ChangeUser.jsp?firstUse'> Informations utilisateur </a></li>
	</ul>
</li>
<li>
<a href='/Devoir2/Deconnexion'>Dconnexion</a>
</li>
</ul>
</nav>
<div class = "wrapper">
<div class = "container">
<h1>Modifier informations</h1><hr><br/>
<form action="/Devoir2/GestionUser" method="post">
<%
if(request.getParameter("firstUse")!=null){ /*ici correspond au pré remplissage du formulaire lorsque l'on va dans le menu pour modifier les infos utilisateurs*/
	String owner = (String) session.getAttribute("login");
	User u = User.findByLogin(ConfigConnection.getUniqueInstance(),owner);
	System.out.println(u.getMail());
%>
<h3> Prénom <br/><br/>
<input type="text" id="frname" name="firstName" required autocomplete="off" value='<%=u.getFiName()%>' /><br/>
<br/> Nom de famille <br/><br/>
<input type="text" id="faname" name="famillyName" required autocomplete="off" value='<%=u.getFaName()%>' /><br/>
<br/> Email <br/><br/> 
<input type="email" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required autocomplete="off" value='<%=u.getMail()%>' /> <br>
<br/> Mot de passe <br/><br/> 
<input type="password" id="psw" name="password" required autocomplete="off" value='<%=u.getPassword()%>' /><br/>
<br/> Homme
<%
if(u.getGender().equals("male")){
	out.println("<input type=\"radio\" id=\"male\" name=\"gender\" value=\"male\" checked/><br/>");
}
else{
	out.println("<input type=\"radio\" id=\"male\" name=\"gender\" value=\"male\"/><br/>");
}
%>
Femme 
<%
if(!u.getGender().equals("female"))
	out.println("<input type=\"radio\" id=\"female\" name=\"gender\" value=\"female\"/><br/>");
else
	out.println("<input type=\"radio\" id=\"female\" name=\"gender\" value=\"female\" checked/><br/>");
%>
<br/><br/>
<%
}
else{ //si la servlet redirige l'utilisateur vers la JSP
%>
<h3> Prénom <br/><br/>
<input type="text" id="frname" name="firstName" required autocomplete="off" value='<%=request.getParameter("firstName")%>' /><br/>
<br/> Nom de famille <br/><br/>
<input type="text" id="faname" name="famillyName" required autocomplete="off" value='<%=request.getParameter("famillyName")%>' /><br/>
<br/> Email <br/><br/>
<%
if(request.getAttribute("changes")!=null){ //si la modification utilisateur a été possible
	out.println("<input type=\"email\" id=\"email\" name=\"email\" value='"+ request.getParameter("email")+"' <br/>");
}
else{
	out.println("<input type=\"email\" id=\"email\" name=\"email\" <br/>");
}
%>
<br/><br/> Mot de passe <br/><br/> 
<input type="password" id="psw" name="password" required autocomplete="off" value='<%=request.getParameter("password")%>' /><br/>
<br/> Homme
<%
if(request.getParameter("gender").equals("male")){
	out.println("<input type=\"radio\" id=\"male\" name=\"gender\" value=\"male\" checked/><br/>");
}
else{
	out.println("<input type=\"radio\" id=\"male\" name=\"gender\" value=\"male\"/><br/>");
}
%>
Femme 
<%
if(!request.getParameter("gender").equals("female"))
	out.println("<input type=\"radio\" id=\"female\" name=\"gender\" value=\"female\"/><br/>");
else
	out.println("<input type=\"radio\" id=\"female\" name=\"gender\" value=\"female\" checked/><br/>");
%>
<br/><br/>
<%
if(request.getAttribute("changes")==null){// si la modification utilisateur n'a pas pu avoir lieu (login déjà existant), on affiche un message d'erreur
%>
<p class="errmsg">L'utilisateur existe dj, essayez une autre adresse mail</p>
<% 
}
else{// si la modification utilisateur a pris lieu, on affiche un message de validation
%>
<p class="msg">Les informations utilisateurs ont bien t enregistres </p>
<%
}
}
%>
</h3>
<input type="submit" value="Modifier"><br/>
</form>
</div>
</div>
</body>
</html>
<%}%>