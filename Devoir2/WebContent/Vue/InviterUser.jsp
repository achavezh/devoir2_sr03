<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ page import="Modele.*"%>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.sql.*" %>
<% // on vérifie que la session est non expirée ou cohérente
if (session.getAttribute("login") == null || session.getAttribute("login").equals("")){
	 response.sendRedirect("Connexion.jsp");
}else{
%>
<html>
<!-- formulaires affichant les utilisateurs que l'on peut inviter ou désinviter -->
<head>
<meta charset="ISO-8859-1">
<title>Inviter utilisateurs</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/design/navigation.css" />
</head>
<body>
<nav>
<!-- menu de navigation -->
<ul>
<li><a href='/Devoir2/Vue/Accueil.jsp'>Accueil</a></li>
<li class="deroulant"><a href='/Devoir2/Vue/Chats.jsp?actual&history'> Propritaire </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Chats.jsp?actual'> Mes Chats </a></li>
		<li><a href='/Devoir2/Vue/Chats.jsp?history'> Historique </a></li>
	</ul>
</li>
<li class="deroulant"><a href='/Devoir2/Vue/Invitations.jsp?actual&history'> Utilisateur </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Invitations.jsp?actual'> Mes Invitations </a></li>
		<li><a href='/Devoir2/Vue/Invitations.jsp?history'> Historique </a></li>
	</ul>
</li>
<li><a href='/Devoir2/Vue/CreerChat.jsp'>Crer un chat</a></li>
<li class="deroulant"><a href='#'> Paramtres </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/ChangeUser.jsp?firstUse'> Informations utilisateur </a></li>
	</ul>
</li>
<li>
<a href='/Devoir2/Deconnexion'>Dconnexion</a>
</li>
</ul>
</nav>
<body>
<div class = "wrapper">
<div class = "container">
<h1>Chats propritaires</h1><hr><br/>
<form method="POST" action="/Devoir2/GestionInvit">
<input type="hidden" name="action" value="invite">
<table border ="1">
<tr>
    <th>#</th>
    <th>Prnom</th>
    <th>Nom</th>
    <th>Mail</th>
    <th>Inviter</th>
</tr>
<%
String owner = (String) session.getAttribute("login");
int id = User.getIdUser(ConfigConnection.getUniqueInstance(), owner);
HashMap<Integer, UserInvited> usersToInvite = User.findUsersToInvite(ConfigConnection.getUniqueInstance(),Integer.parseInt(request.getParameter("id_chat")),id); // on récupère l'ensemble des utilisateurs qui peuvent être invités ou désinvités
for(Map.Entry<Integer, UserInvited> u : usersToInvite.entrySet()){
%>
<tr>
	<td><%=u.getKey()%></td>
	<td><%=u.getValue().user.getFiName()%></td>
	<td><%=u.getValue().user.getFaName()%></td>
	<td><%=u.getValue().user.getMail()%></td>
	<td>
<%
	if(u.getValue().inChat==true){ // si l'utilisateur est déjà invité on le coche
%>
		<input type="checkbox" name="usersList" value=<%=u.getKey()%> checked>
<%
	}
	else{
		
%>
		<input type="checkbox" name="usersList" value=<%=u.getKey()%>>
<%
	}
%>
	<!-- on ajoute une case invisible qui permet de renvoyer à la servlet la liste entière des utilisateurs affichés et pas seulement les utilisateurs cochés --> 
	<input type="hidden" name="usersListVerif" value=<%=u.getKey()%> checked></td>
</tr>
<% 
}%>
</table><br/><br/>
<input type="hidden" name="id_chat" value=<%=request.getParameter("id_chat")%>>
<%
if(!usersToInvite.isEmpty()){ // on affiche le bouton seulement si des utilisateurs sont disponibles dans la liste
%>
<input type="submit" value="Valider les choix d'utilisateur"></input>
<%
}%>
</form>
<%
if(request.getParameter("goodSelect")!=null) /*affichage d'un message de validation lorsque la servlet nous renvoie vers la JSP et que les utilisateurs ont bien été invités/ désinvités */
	out.println("<p class=\"msg\">Les changements ont bien t pris en compte.</p><br/><br/>");
%>
</div>
</div>
<footer>
  <p>Authors: Andrea Chavez et Emilien Carrez</p>
</footer>
</body>
</html>
<%}%>