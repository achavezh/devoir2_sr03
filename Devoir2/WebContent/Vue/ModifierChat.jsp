<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<% 
if (session.getAttribute("login") == null || session.getAttribute("login").equals("")){
	 response.sendRedirect("Connexion.jsp");
}else{
%>
<!DOCTYPE html>
<html>
<!-- formulaire de modification des informations d'un chat -->
<head>
<meta charset="ISO-8859-1">
<title>Modifier Chat</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/design/navigation.css" />
</head>
<body>
<nav>
<!-- menu de navigation -->
<ul>
<li><a href='/Devoir2/Vue/Accueil.jsp'>Accueil</a></li>
<li class="deroulant"><a href='/Devoir2/Vue/Chats.jsp?actual&history'> Propritaire </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Chats.jsp?actual'> Mes Chats </a></li>
		<li><a href='/Devoir2/Vue/Chats.jsp?history'> Historique </a></li>
	</ul>
</li>
<li class="deroulant"><a href='/Devoir2/Vue/Invitations.jsp?actual&history'> Utilisateur </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Invitations.jsp?actual'> Mes Invitations </a></li>
		<li><a href='/Devoir2/Vue/Invitations.jsp?history'> Historique </a></li>
	</ul>
</li>
<li><a href='/Devoir2/Vue/CreerChat.jsp'>Crer un chat</a></li>
<li class="deroulant"><a href='#'> Paramtres </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/ChangeUser.jsp?firstUse'> Informations utilisateur </a></li>
	</ul>
</li>
<li>
<a href='/Devoir2/Deconnexion'>Dconnexion</a>
</li>
</ul>
</nav>
<div class = "wrapper">
<div class = "container">
<h1>Modifier Chat</h1><hr><br/>
<form action="/Devoir2/ChatChanges?modify" method="post">
<input type="hidden" name="id_chat" value='<%=request.getParameter("id_chat")%>'>

<h3>Titre <br/><br/>
<input type="text" id="titre" name="titre" maxlength="100" autocomplete="off" required value="<%=request.getParameter("titre")%>" /><br/>
<br/>Description <br/><br/>
<input type="text" id="description" name="description" maxlength="400" autocomplete="off" value="<%=request.getParameter("description")%>" /><br/>

<br/>Date<br/><br/>
<%
if(request.getAttribute("pastChosen")==null){ /* si le chat a été modifié pour être dans le passé, la servlet nous a renvoyé vers le JSP avec un attribut d'erreur qui n'affichera pas la date précédemment entrée car elle est invalide */ 
%>
<input type="date" id="date" name="date" required autocomplete="off" value=<%=request.getParameter("date")%> /><br/>
<%
}
else{
%>
<input type="date" id="date" name="date" required autocomplete="off" /><br/>
<%}

%>
<br/>Horaire <br/><br/>
<%
if(request.getAttribute("pastChosen")==null){ /* si le chat a été modifié pour être dans le passé, la servlet nous a renvoyé vers le JSP avec un attribut d'erreur qui n'affichera pas l'horaire précédemment entré car il est invalide */ 
%>
<input type="time" id="horaire" name="horaire" autocomplete="off" required value=<%=request.getParameter("horaire")%> /><br/>
<%
}
else{
%>
<input type="time" id="horaire" name="horaire" autocomplete="off" required /><br/>
<%}

%>
<br/>Duree<br/><br/>
<input type="time" id="duree" name="duree" required autocomplete="off" value=<%=request.getParameter("duree")%> /><br/>

</h3> 
<input type="submit" value="Modifier"><br/>
</form>
<%
if(request.getAttribute("pastChosen")!=null){ // si le chat a été modifié pour être défini dans le passé
%>
<p class=errmsg> Veuillez choisir une date ultrieure  la date actuelle </p>
<%
}%>
</div>
</div>
<footer>
  <p>Authors: Andrea Chavez et Emilien Carrez</p>
</footer>
</body>
</html>
<%}%>