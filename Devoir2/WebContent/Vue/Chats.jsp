<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="Modele.*"%>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.sql.*" %>
<% 
if (session.getAttribute("login") == null || session.getAttribute("login").equals("")){
	 response.sendRedirect("Connexion.jsp");
}else{
%>
<html>
<!-- affiche les chats propriétaires de l'utilisateur -->
<head>
<meta charset="ISO-8859-1">
<title>Chats</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/design/navigation.css" />
</head>
<body>
<nav>
<!-- menu de navigation -->
<ul>
<li><a href='/Devoir2/Vue/Accueil.jsp'>Accueil</a></li>
<li class="deroulant"><a href='/Devoir2/Vue/Chats.jsp?actual&history'> Propritaire </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Chats.jsp?actual'> Mes Chats </a></li>
		<li><a href='/Devoir2/Vue/Chats.jsp?history'> Historique </a></li>
	</ul>
</li>
<li class="deroulant"><a href='/Devoir2/Vue/Invitations.jsp?actual&history'> Utilisateur </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Invitations.jsp?actual'> Mes Invitations </a></li>
		<li><a href='/Devoir2/Vue/Invitations.jsp?history'> Historique </a></li>
	</ul>
</li>
<li><a href='/Devoir2/Vue/CreerChat.jsp'>Crer un chat</a></li>
<li class="deroulant"><a href='#'> Paramtres </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/ChangeUser.jsp?firstUse'> Informations utilisateur </a></li>
	</ul>
</li>
<li>
<a href='/Devoir2/Deconnexion'>Dconnexion</a>
</li>
</ul>
</nav>
<div class = "wrapper">
<div class = "container">
<%
if(request.getParameter("actual")!=null){ // on affiche les chats non expirées par la date 
%>
<h1>Vos Chats</h1><hr><br/><br/>
<table border ="1">
<tr>
    <th>#</th>
    <th>Titre</th>
    <th>Description</th>
    <th>Date</th>
    <th>Horaire</th>
    <th>Duree</th>
    <th>Modifier</th>
    <th>Supprimer</th>
</tr>
<%
String owner = (String) session.getAttribute("login");
int id = User.getIdUser(ConfigConnection.getUniqueInstance(), owner); // on récupère l'id de l'utilisateur propriétaires
HashMap<Integer, Chat> chatsOwned;
chatsOwned = Chat.findByOwner(id,true); // on récupère les chats non expirées par la date
for(Map.Entry<Integer, Chat> c : chatsOwned.entrySet()){
%>
<tr>
	<td><%=c.getKey()%></td>
	<td><%=c.getValue().getTitre()%></td>
	<td><%=c.getValue().getDescription()%></td>
	<td><%=c.getValue().getDate()%></td>
	<td><%=c.getValue().getHoraire()%></td>
	<td><%=c.getValue().getDuree()%></td>
	<td> <!-- on ajoute un bouton pour modifier les infos du chat, et un autre pour le supprimer -->
		<form action="/Devoir2/Vue/ModifierChat.jsp" method="post">
		<input type="hidden" name="id_chat" value=<%=c.getKey()%>>
		<input type="hidden" name="titre" value="<%=c.getValue().getTitre()%>">
		<input type="hidden" name="description" value="<%=c.getValue().getDescription()%>">
		<input type="hidden" name="date" value=<%=c.getValue().getDate()%>>
		<input type="hidden" name="horaire" value=<%=c.getValue().getHoraire()%>>
		<input type="hidden" name="duree" value=<%=c.getValue().getDuree()%>>
		<input type="submit" value="Modifier"></input></form>
	</td>
	<td>
		<form action="/Devoir2/ChatChanges?suppr" method="post">
		<input type="hidden" name="id_chat" value=<%=c.getKey()%>>
		<input type=Submit value="Supprimer"></form>
	</td>

</tr>
<%
	}
}
%>
</table>
<%
if(request.getParameter("history")!=null){ // on veut afficher les chats propriétaires de l'utilisateur qui sont expirés
%>
<h1>Historique des Chats</h1><hr><br/><br/>
<table border ="1">
<tr>
    <th>#</th>
    <th>Titre</th>
    <th>Description</th>
    <th>Date</th>
    <th>Horaire</th>
    <th>Duree</th>
</tr>
<%
String owner = (String) session.getAttribute("login");
int id = User.getIdUser(ConfigConnection.getUniqueInstance(), owner);
HashMap<Integer, Chat> chatsOwned;
chatsOwned = Chat.findByOwner(id,false); // on récupère les chats propriétaires de l'utilisateur qui sont expirés
for(Map.Entry<Integer, Chat> c : chatsOwned.entrySet()){
%>
<tr>
	<td><%=c.getKey()%></td>
	<td><%=c.getValue().getTitre()%></td>
	<td><%=c.getValue().getDescription()%></td>
	<td><%=c.getValue().getDate()%></td>
	<td><%=c.getValue().getHoraire()%></td>
	<td><%=c.getValue().getDuree()%></td>

</tr>
<%
	}
}
%>
</table>
</div>
</div>
<footer>
  <p>Authors: Andrea Chavez et Emilien Carrez</p>
</footer>
</body>
</html>
<%}%>