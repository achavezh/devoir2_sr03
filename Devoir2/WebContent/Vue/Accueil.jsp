<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ page import="Modele.*"%>
<%@ page import="java.util.*" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.time.LocalTime" %>
<% 
//on teste si la session est valide
if (session.getAttribute("login") == null || session.getAttribute("login").equals("")){
	 response.sendRedirect("Connexion.jsp");
}else{
%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Accueil</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/design/navigation.css" />
</head>
<body>
<!-- menu de navigation -->
<nav>
<ul>
<li><a href='/Devoir2/Vue/Accueil.jsp'>Accueil</a></li>
<li class="deroulant"><a href='/Devoir2/Vue/Chats.jsp?actual&history'> Propritaire </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Chats.jsp?actual'> Mes Chats </a></li>
		<li><a href='/Devoir2/Vue/Chats.jsp?history'> Historique </a></li>
	</ul>
</li>
<li class="deroulant"><a href='/Devoir2/Vue/Invitations.jsp?actual&history'> Utilisateur </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/Invitations.jsp?actual'> Mes Invitations </a></li>
		<li><a href='/Devoir2/Vue/Invitations.jsp?history'> Historique </a></li>
	</ul>
</li>
<li><a href='/Devoir2/Vue/CreerChat.jsp'>Crer un chat</a></li>
<li class="deroulant"><a href='#'> Paramtres </a>
	<ul class="sous">
		<li><a href='/Devoir2/Vue/ChangeUser.jsp?firstUse'> Informations utilisateur </a></li>
	</ul>
</li>
<li>
<a href='/Devoir2/Deconnexion'>Dconnexion</a>
</li>
</ul>
</nav>
<div class = "wrapper">
<div class = "container">
<h1>Bienvenue 
<%
StringTokenizer ST = new StringTokenizer(session.getAttribute("login").toString(),"@");
String log =  ST.nextToken();
out.println(log);
%> 
</h1><hr>
<h3>Chats propritaires</h3>
<!-- on affiche les chats propriétaires -->
<table border ="1">
<tr>
    <th>#</th>
    <th>Titre</th>
    <th>Description</th>
    <th>Date</th>
    <th>Horaire</th>
    <th>Duree</th>
    <th>Inviter/Supprimer Utilisateurs</th>
    <th>Salle de Chat</th>
</tr>
<%
String owner = (String) session.getAttribute("login");
int id = User.getIdUser(ConfigConnection.getUniqueInstance(), owner); //on récupère l'id utilisateur
HashMap<Integer, Chat> chatsOwned = Chat.findByOwner(id,true); //on récupère les chats propriétaires non expirés
for(Map.Entry<Integer, Chat> c : chatsOwned.entrySet()){
%>
<tr>
	<td><%=c.getKey()%></td>
	<td><%=c.getValue().getTitre()%></td>
	<td><%=c.getValue().getDescription()%></td>
	<td><%=c.getValue().getDate()%></td>
	<td><%=c.getValue().getHoraire()%></td>
	<td><%=c.getValue().getDuree()%></td>
	<td> <!-- on affiche un bouton permettant d'accéder à la gestion des utilisateurs -->
		<form action="${pageContext.request.contextPath}/Vue/
InviterUser.jsp?id_chat=<%=c.getKey()%>" method="post" target="_blank">
		<input type=Submit value="Gérer les utilisateurs">
		</form>
	<td>
	<%
	/*on découpe la date/horaire/durée et on les assemble en objet Calendar afin de tester si le chat est dans sa durée de fonctionnement, si oui on affiche un bouton pour accéder à la salle de chat*/
	String date = c.getValue().getDate();
	String horaire = c.getValue().getHoraire();
	String duree = c.getValue().getDuree();
	
	StringTokenizer S = new StringTokenizer(date, "-");
	String year = S.nextToken();
	String month = S.nextToken();
	String day = S.nextToken();
	
	S = new StringTokenizer(horaire, ":");
	String hourH = S.nextToken();
	String minutesH = S.nextToken();
	
	S= new StringTokenizer(duree,":");
	String hourD = S.nextToken();
	String minutesD = S.nextToken();
	Calendar cal=Calendar.getInstance();
	cal.set(Integer.parseInt(year), Integer.parseInt(month)-1, Integer.parseInt(day), Integer.parseInt(hourH), Integer.parseInt(minutesH),0);
	Date dInf=cal.getTime();
	cal.set(Integer.parseInt(year), Integer.parseInt(month)-1, Integer.parseInt(day), Integer.parseInt(hourH)+Integer.parseInt(hourD),Integer.parseInt(minutesH)+Integer.parseInt(minutesD),0);
	Date dSup=cal.getTime();
	if (Calendar.getInstance().getTime().equals(dInf) || Calendar.getInstance().getTime().equals(dSup) || (Calendar.getInstance().getTime().after(dInf) && Calendar.getInstance().getTime().before(dSup))) {
	%>
		<form action="SalleDeChat.jsp" method="post" target="_blank">
		<input type="hidden" name="id_chat" value=<%=c.getKey()%>>
		<input type="hidden" name="titre_chat" value='<%=c.getValue().getTitre()%>'>
		<input type="hidden" name="date" value='<%=c.getValue().getDate()%>'>
		<input type="hidden" name="horaire" value='<%=c.getValue().getHoraire()%>'>
		<input type="hidden" name="duree" value='<%=c.getValue().getDuree()%>'>
		<input type=Submit value="Salle de chat">
		</form>
	<%
	}
	else{
	%>
	<p> La salle de chat<br/> n'est pas encore disponible</p>
	<%
	}%>
	</td>
</tr>
<%
}%>
</table>
<h3>Chats utilisateurs</h3>
<!-- on affiche les chats utilisateurs -->
<table border ="1">
<tr>
    <th>#</th>
    <th>Titre</th>
    <th>Description</th>
    <th>Date</th>
    <th>Horaire</th>
    <th>Duree</th>
    <th>Salle de Chat</th>
</tr>
<%
HashMap<Integer, Chat> chatsInvited = Chat.findByIDUser(id,true,true); //on cherche les chats utilisateurs non expirés
for(Map.Entry<Integer, Chat> co : chatsInvited.entrySet()){
%>
<tr>
	<td><%=co.getKey()%></td>
	<td><%=co.getValue().getTitre()%></td>
	<td><%=co.getValue().getDescription()%></td>
	<td><%=co.getValue().getDate()%></td>
	<td><%=co.getValue().getHoraire()%></td>
	<td><%=co.getValue().getDuree()%></td>
	<td>
	<%
	/*on découpe la date/horaire/durée et on les assemble en objet Calendar afin de tester si le chat est dans sa durée de fonctionnement, si oui on affiche un bouton pour accéder à la salle de chat*/
	String date = co.getValue().getDate();
	String horaire = co.getValue().getHoraire();
	String duree = co.getValue().getDuree();
	
	StringTokenizer S = new StringTokenizer(date, "-");
	String year = S.nextToken();
	String month = S.nextToken();
	String day = S.nextToken();

	S = new StringTokenizer(horaire, ":");
	String hourH = S.nextToken();
	String minutesH = S.nextToken();
	
	S= new StringTokenizer(duree,":");
	String hourD = S.nextToken();
	String minutesD = S.nextToken();
	
	Calendar cal=Calendar.getInstance();
	cal.set(Integer.parseInt(year), Integer.parseInt(month)-1, Integer.parseInt(day), Integer.parseInt(hourH), Integer.parseInt(minutesH),0);
	Date dInf=cal.getTime();
	cal.set(Integer.parseInt(year), Integer.parseInt(month)-1, Integer.parseInt(day), Integer.parseInt(hourH)+Integer.parseInt(hourD),Integer.parseInt(minutesH)+Integer.parseInt(minutesD),0);
	Date dSup=cal.getTime();
	if (Calendar.getInstance().getTime().equals(dInf) || Calendar.getInstance().getTime().equals(dSup) || (Calendar.getInstance().getTime().after(dInf) && Calendar.getInstance().getTime().before(dSup))) {
	%>
		<form action="SalleDeChat.jsp" method="post" target="_blank">
		<input type="hidden" name="id_chat" value=<%=co.getKey()%>>
		<input type="hidden" name="titre_chat" value='<%=co.getValue().getTitre()%>'>
		<input type="hidden" name="date" value='<%=co.getValue().getDate()%>'>
		<input type="hidden" name="horaire" value='<%=co.getValue().getHoraire()%>'>
		<input type="hidden" name="duree" value='<%=co.getValue().getDuree()%>'>
		<input type=Submit value="Salle de chat">
		</form>
	<%
	}
	%>
	</td>
</tr>
<%
}%>
</table>
</div>
</div>
<footer>
  <p>Authors: Andrea Chavez et Emilien Carrez</p>
</footer>
</body>
</html>
<%}%>