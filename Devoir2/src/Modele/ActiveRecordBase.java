package Modele;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * Classe abstrait qui aide à construire les comportaments en commun de ces classes filles
 * La classe communique avec la base de donnes.
 * */
public abstract class ActiveRecordBase {
	
	//_builtFromDB : pour savoir si l'objet est persistant. 
	protected boolean _builtFromDB;

	//mthodes get et set de _builtFromDB
	public void setBoolean(boolean b) {
		_builtFromDB=b;
	}
	
	public boolean getBuiltFromDB() {
		return _builtFromDB;
	}
	
	/****Mthodes protected abstract pour les redefinir dans les classes filles****/
	protected abstract String _update();
	protected abstract String _insert();
	protected abstract String _delete();
	
	/****Méthodes en commun des classes filles****/

	/**
	 * 1 . delete() : est appelé quand on souhaite supprimer un objet de la base de donnes 
	 * elle fait la surpression si l'objet est présent dans la base de donnes.
	 * **/
	public int delete(Connection cx) throws SQLException{
		Statement s=cx.createStatement();
		int nbRowsAffected=0;
		if(_builtFromDB) {
			System.out.println("Executing this command:"+_delete()+"\n");
			nbRowsAffected=s.executeUpdate(_delete());
		}
		else {
			System.out.println("Objet non persistant");
		}
		return nbRowsAffected;
		
	}
	/**
	 * 1 . save() : est appelée quand on souhaite insérer ou modifier un objet dans la base de données 
	 * 		a) elle insere un tuple si l'objet n'est pas construit par la base de donnes.
	 * 		b) elle modifie un tuple si l'objet est construit par la base de donnes. 
	 * **/
	public int save(Connection cx) throws SQLException{
		Statement s=cx.createStatement();
		int nbRowsAffected=0;
		if(_builtFromDB) {
			System.out.println("Execution this command: "+_update());
			nbRowsAffected=s.executeUpdate(_update());
		}
		else {
			System.out.println("Executing this command: "+_insert());
			nbRowsAffected=s.executeUpdate(_insert());
			_builtFromDB=true;
		}
		return nbRowsAffected;
	}

}