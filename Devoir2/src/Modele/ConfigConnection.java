package Modele;

import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import java.sql.*;
/*
 * Classe abstrait qui aide à retourner une instance unique de la Configuration de la classe.
 * La classe suivra le design pattern Singleton.
 * */
public class ConfigConnection {
	
	private static Connection UniqueInstance; //attribut prive qui donnera l'instance unique
	
	
	private ConfigConnection() throws ClassNotFoundException, SQLException, IOException {
	 	
        Properties param = new Properties();
        URL urlFichierProp = ConfigConnection.class.getResource("fichierProp.properties"); //on obtient le fichier properties
        //On gère les exceptions si le fichier est null
        if (urlFichierProp == null) {
            throw new IOException("Fichier " + "BDparam" + " pas trouvé !");
        }
        BufferedInputStream bis = null;
        try {
        	//On ouvre le InputStream pour lire le fichier properties
            bis = new BufferedInputStream(urlFichierProp.openStream());
            param.load(bis);
            //On garde chaque information obtenue :
            String driver = param.getProperty("driver");
            String url = param.getProperty("url");
            String utilisateur = param.getProperty("user");
            String mdp = param.getProperty("psw");
            Class.forName(driver);
            //On initialise l'attribut
            UniqueInstance=DriverManager.getConnection(url, utilisateur, mdp);
        } finally {
            if (bis != null) {
                bis.close();
            }
        }
		
	}
	
	/**Singleton
	 * @throws IOException 
	 * @throws SQLException 
	 * @throws ClassNotFoundException **/
	public static Connection getUniqueInstance() throws ClassNotFoundException, SQLException, IOException {
		if(UniqueInstance == null) {
			new ConfigConnection();
		}
		return UniqueInstance;
	}
	

}