package Modele;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/*
 * Classe fils qui construit les Objets de type Correspondance (association entre Chat et User)
 * */
public class CorrespClientChat extends ActiveRecordBase {

	private int id_cli;
	private int id_chat;
	//invited : Attribut qui indique si l'utilisateur a accepté l'invitation ou si celle-ci est encore en attente 
	private boolean invited=false;

    //constructeur par défaut
	public CorrespClientChat() {
		_builtFromDB = false;
	}

	/*Constructeur pour créer un nouvelle objet construit à partir d'une requête auprès de la base de donnes :*/	
	public CorrespClientChat(ResultSet rs) throws SQLException {
		id_cli = rs.getInt(2);
		id_chat = rs.getInt(1);
		invited=rs.getBoolean(3);
		_builtFromDB = true;
	}
	/*Constructeur pour créer un nouvelle objet :*/
	public CorrespClientChat(int idCli,int idChat,boolean i) {
		id_cli=idCli;
		id_chat=idChat;
		invited=i;
	}
	
	//Méthodes get et set des attributs 
	public void setInvited(boolean b) {
		invited=b;
	}
	
	public void setCli(int i) {
		id_cli=i;
	}
	
	public void setChat(int i) {
		id_chat=i;
	}
	
	public boolean getInvited() {
		return invited;
	}
	
	public int getCli() {
		return id_cli;
	}
	
	public int getChat() {
		return id_chat;
	}

	/**
	 * Définition du comportement respectif des méthodes abstraites déclarés dans la classe mère 
	 * _update() : Retourne la requête pour changer les attributs d'une correspondance par rapport à l'id de l'utilisateur et  à l'id du chat
	 * _insert() : Retourne la requte pour insrer un objet Chat dans la base de donnes. 
	 * _delete() : Retourne la requte pour supprimer un Chat par rapport  son id. 
	 */
	protected String _update() {
		return "UPDATE correspondance SET invited="+invited+" WHERE ID_cli=" + id_cli+" AND ID_chat="+id_chat;
	}

	protected String _insert() {
		return "INSERT INTO correspondance (ID_cli,ID_chat,invited) VALUES ("+id_cli+","+id_chat+","+invited+")";
	}

	protected String _delete() {
		return "DELETE FROM correspondance WHERE ID_Cli= " + id_cli+" AND ID_chat="+id_chat;
	}

	/**
	 * Mthode qui retourne tous les tuples de la table correspodance dans la BDD
	 * */
	public static List<CorrespClientChat> findAll(Connection cx) throws SQLException{
		Statement s=cx.createStatement();
		ResultSet res=s.executeQuery( "SELECT * FROM correspondance");
		List <CorrespClientChat> all=new ArrayList<CorrespClientChat>();
		while(res.next()) {
			CorrespClientChat d = new CorrespClientChat(res.getInt("ID_cli"), res.getInt("ID_chat"),res.getBoolean("invited"));
			all.add(d);
		}
		return all;
	}
	/**
	 * Méthode qui retourne le tuple de la table correspondance qui correspond à l'id chat et l'id client envoyé
	 * */
	public static CorrespClientChat findByIDS(Connection cx, int id_cli, int id_chat) throws SQLException{
		Statement s=cx.createStatement();
		ResultSet res=s.executeQuery("SELECT * FROM correspondance WHERE ID_chat="+id_chat+" AND ID_cli="+id_cli);
		while(res.next()) {
			CorrespClientChat n=new CorrespClientChat(res);
			return n;
		}
		return null;
	}
}
