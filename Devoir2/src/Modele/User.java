package Modele;

import java.io.IOException;
import java.lang.*;
import java.sql.*;
import java.sql.SQLException;
import java.util.*;

public class User extends ActiveRecordBase {
	private int id;
	private String Fi_name;
	private String Fa_name;
	private String mail;
	private String password;
	private String gender;

	//constructeur par défaut
	public User() {
		_builtFromDB = false;
	}

	/*Constructeur pour créer un nouvelle objet construit à partir d'une requête auprès de la base de donnes :*/
	public User(ResultSet rs) throws SQLException {
		id = rs.getInt(1);
		gender = rs.getString(6);
		password = rs.getString(5);
		mail = rs.getString(4);
		Fi_name = rs.getString(2);
		Fa_name = rs.getString(3);
		_builtFromDB = true;
	}

	//constructeur permettant de créer un objet à partir des attributs passés en paramètres
	public User(String fi, String fa, String m, String p, String g) {
		Fi_name = fi;
		Fa_name = fa;
		mail = m;
		password = p;
		gender = g;
	}

	//méthode set et get de chaque attribut
	public void setFiName(String f) {
		Fi_name = f;
	}

	public void setFaName(String f) {
		Fa_name = f;
	}

	public void setMail(String f) {
		mail = f;
	}

	public void setPassword(String f) {
		password = f;
	}

	public void setGender(String f) {
		gender = f;
	}

	public String getFiName() {
		return Fi_name;
	}

	public String getFaName() {
		return Fa_name;
	}

	public String getMail() {
		return mail;
	}

	public String getPassword() {
		return password;
	}

	public String getGender() {
		return gender;
	}

	public String toString() {
		return "user{" + "Family Name=" + Fa_name + ", First Name=" + Fi_name + "" + ", gender=" + gender + ","
				+ " pwd=" + password + '}';
	}

	/**
	 * Définition du comportement respectif des méthodes abstraites déclarés dans la classe mère. 
	 * _update() : Retourne la requête pour changer les attributs d'un User par rapport à l'id de l'utilisateur
	 * _insert() : Retourne la requte pour insérer un objet Chat dans la base de donnes. 
	 * _delete() : Retourne la requte pour supprimer un Chat par rapport  son id. 
	 */

	protected String _update() {
		return "UPDATE user SET FirstName='" + Fi_name.replace("'", "\\'") + "', FamilyName='" + Fa_name.replace("'", "\\'") + "', Mail='" + mail.replace("'", "\\'")
				+ "', Password='" + password.replace("'", "\\'") + "',Genre='" + gender + "' WHERE ID=" + id;
	}

	protected String _insert() {
		return "INSERT INTO user (FirstName,FamilyName,Mail,Password,Genre) VALUES ('" + Fi_name.replace("'", "\\'") + "','" + Fa_name.replace("'", "\\'")
				+ "','" + mail.replace("'", "\\'") + "','" + password.replace("'", "\\'") + "','" + gender + "')";
	}

	protected String _delete() {
		return "DELETE FROM user" + " WHERE ID= " + id;
	}

	//fonction permettant de retourner un utilisateur par rapport à son login/mdp
	public static boolean findByLoginAndPwd(Connection cx, String login, String pwd) throws SQLException {
		Statement s = cx.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM user WHERE Mail='" + login.replace("'", "\\'") + "' AND Password='" + pwd.replace("'", "\\'") + "'");
		return res.next();
		
	}
	
	//fonction permettant de retourner un utiisateur par rapport à son login
	public static User findByLogin(Connection cx, String login) throws SQLException {
		Statement s = cx.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM user WHERE Mail='" + login.replace("'", "\\'") +"'");
		if(res.next())
			return new User(res);
		else
			return null;
		
	}

	//fonction permettant de retourner un utilisateur par rapport à son id
	public static User findByID(Connection cx, int id) throws SQLException {
		Statement s = cx.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM user WHERE ID=" + id);
		while (res.next()) {
			User d = new User(res); //utilisateur complété avec les infos de la requête
			return d;// on retourne l'utilisateur
		}
		return new User();
	}

	//fonction permettant de retourner tous les utilisateurs définit dans la BDD
	public static HashMap<Integer, User> findAll(Connection cx) throws SQLException {
		Statement s = cx.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM user");
		HashMap<Integer, User> all = new HashMap<Integer, User>();
		while (res.next()) {// on parcourt la requête et on ajoute chaque utilisateur dans la HashMap
			User d = new User(res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6));
			all.put(res.getInt("ID"), d);
		}
		return all;
	}
	
	/*fonction permettant de renvoyer la liste possible des utilisateurs à inviter ou à désinviter (tous les utilisateurs sauf le propriétaire et les utilisateurs en attente d'avoir accepté l'invitation déjà effectuée)*/
	public static HashMap<Integer, UserInvited> findUsersToInvite(Connection cx,int id_chat,int id_cli)
			throws SQLException, ClassNotFoundException, IOException {
		Statement s = cx.createStatement();
		ResultSet res = s.executeQuery("SELECT ID,FirstName,FamilyName, Mail, Password, Genre, invited FROM user\r\n"
				+ "LEFT JOIN \r\n (SELECT * FROM correspondance WHERE correspondance.ID_chat=+" + id_chat
				+ ")\r\n AS userInvited ON userInvited.ID_cli=user.ID WHERE (invited=true OR invited is NULL) AND user.ID<>"+id_cli);
		HashMap<Integer, UserInvited> all = new HashMap<Integer, UserInvited>();
		while (res.next()) { // on parcourt le résultat de la requête
			User d = new User(res.getString("FirstName"), res.getString("FamilyName"), res.getString("Mail"),
					res.getString("Password"), res.getString("Genre"));
			UserInvited data=new UserInvited(); // on créer un UserInvited
			data.user=d;
			if(res.getBoolean("invited")==true){ // si l'utilisateur est invité et qu'il a accepté l'invitation on lui met son boolean invité (inChat) à true
				data.inChat=true;
			}
			else {
				data.inChat=false;//sinon à faux
			}
			all.put(res.getInt("ID"), data);
		}
		return all;
	}

	//fonction permettant de retourner un boolean indiquant si un utilisateur existe dans la BDD par rapport à son login
	public static boolean userExistante(Connection cx, String login) throws SQLException {
		Statement s = cx.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM user WHERE Mail='" + login.replace("'", "\\'") + "'");
		return res.next();
	}

	//fonction permettant de renvoyer l'id d'un utilisateur par rapport à son login
	public static int getIdUser(Connection cx, String login) throws SQLException {
		Statement s = cx.createStatement();
		System.out.println("SELECT ID FROM user WHERE Mail='" + login.replace("'", "\\'") + "'");
		ResultSet res = s.executeQuery("SELECT ID FROM user WHERE Mail='" + login.replace("'", "\\'") + "'");
		int all = -1; // renvoie -1 si aucun utilisateur avec ce login
		while (res.next()) {
			all = res.getInt("ID"); // renvoie l'ID sinon (il n'y en aura qu'un dans tous les cas)
		}
		return all;
	}
}