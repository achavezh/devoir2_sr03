package Modele;

import java.io.IOException;

import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Date;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
/*
 * Classe fils qui aide à construire les Objets de type Chat
 * */
public class Chat extends ActiveRecordBase {

	private int id;
	private String titre;
	private String description;
	private String date;
	private String horaire;
	private String duree;
	private int id_owner; //attribut qui garde l'id de l'utilisateur qui est propiètaire du chat

	/*Constructeur pour créér un nouvelle objet :*/
	public Chat(String titre, String description, String date, String horaire, String duree, int prop) {
		this.titre = titre;
		this.description = description;
		this.date = date;
		this.horaire = horaire;
		this.duree = duree;
		this.id_owner = prop;
	}
	/*Constructeur pour créér un nouvelle objet construit à partir d'une requête de la base de données :*/
	public Chat(ResultSet rs) throws SQLException {
		id = rs.getInt("id_chat");
		titre = rs.getString("titre");
		description = rs.getString("description");
		date = rs.getString("date");
		horaire = rs.getTime("horaire").toString();
		duree = rs.getTime("duree").toString();
		id_owner = rs.getInt("owner");
		_builtFromDB = true;
	}

	public Chat() {
	}

	//Méthodes get et set des attributs 
	
	public void setTitre(String Titre) {
		titre = Titre;
	}

	public void setDescription(String Description) {
		description = Description;
	}

	public void setDate(String d) {
		date = d;
	}

	public void setHoraire(String Horaire) {
		horaire = Horaire;
	}

	public void setDuree(String Duree) {
		duree = Duree;
	}

	public void setOwner(int id) {
		id_owner = id;
	}

	public String getTitre() {
		return titre;
	}

	public String getDescription() {
		return description;
	}

	public String getDate() {
		return date;
	}

	public String getHoraire() {
		return horaire;
	}

	public String getDuree() {
		return duree;
	}

	public int getOwner() {
		return id_owner;
	}

	/**
	 * Définition du comportement respectif des méthodes abstraits déclarés dans la classe mère. 
	 * _update() : Retourne la requête pour changer les attributs d'un Chat par rapport à son id
	 * _insert() : Retourne la requête pour insèrer un objet Chat dans la base de données. 
	 * _delete() : Retourne la requête pour supprimer un Chat par rapport à son id. 
	 */
	@Override
	protected String _update() {
		return "UPDATE chat SET titre='" + titre.replace("'", "\\'") + "', description ='" + description.replace("'", "\\'") + "', date='" + date
				+ "', horaire='" + horaire + "', duree='" + duree + "', owner=" + id_owner + " WHERE id_chat='" + id
				+ "'";
	}

	@Override
	protected String _insert() {
		return "INSERT INTO chat (titre, description, date, horaire, duree, owner) VALUES ('" + titre.replace("'", "\\'") + "','"
				+ description.replace("'", "\\'") + "', '" + date + "','" + horaire + "'" + ", '" + duree + "'," + id_owner + ")";
	}

	@Override
	protected String _delete() {
		return "DELETE FROM chat" + " WHERE id_chat= " + id;
	}

	/**
	 * Méthode qui retourne un objet Chat en le cherchant dans la basse de données avec son id
	 * */
	public static Chat findByID(Connection cx, int id) throws SQLException {
		Statement s = cx.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM chat WHERE id_chat=" + id);
		if (res.next()) {
			Chat d = new Chat(res);
			return d;
		}
		return null;
	}
	/**
	 * Méthode qui retourne un HashMap contenant les Chats créés par l'utilisateur 
	 * */
	
	public static HashMap<Integer, Chat> findByOwner(int id, boolean actual)
			throws SQLException, ClassNotFoundException, IOException {
		Connection cx = ConfigConnection.getUniqueInstance();
		Statement s = cx.createStatement();
		ResultSet res;
		/**
		 * Si le boolean recu est true, on veut donc obtenir les chats actuelles, cad les chats
		 * qui sont ne sont pas expirés, trouvés grâce à ">NOW()" (Mes chats)
		 * */
		if (actual == true) {
			System.out.println("SELECT * FROM chat WHERE owner=" + id
					+ " AND ADDTIME(TIMESTAMP(date,horaire),duree)>NOW()");
			res = s.executeQuery("SELECT * FROM chat WHERE owner=" + id
					+ " AND ADDTIME(TIMESTAMP(date,horaire),duree)>NOW()");
			/**
			 * Si le boolean recu est false, on veut obtenir les chats expirés, trouvés grâce à "<=NOW()" (Historique de Mes Chats)
			 * */
		} else {
			System.out.println("SELECT * FROM chat WHERE owner=" + id
					+ " AND ADDTIME(TIMESTAMP(date,horaire),duree)<=NOW()");
			res = s.executeQuery("SELECT * FROM chat WHERE owner=" + id
					+ " AND ADDTIME(TIMESTAMP(date,horaire),duree)<=NOW()");
		}
		HashMap<Integer, Chat> all = new HashMap<Integer, Chat>();
		//On utilise le while pour parcourir tous les résultats de la requête et ajouter les chats dans la HashMap
		while (res.next()) {
			Chat d = new Chat(res.getString("titre"), res.getString("description"), res.getString("date"),
					res.getString("horaire"), res.getString("duree"), res.getInt("owner"));
			all.put(res.getInt("id_chat"), d);
		}
		return all;
	}

	/**
	 * Méthode qui retourne un HashMap contenant les Chats où l'utilisateur a été invité, par rapport à l'id client et à un booléen indiquant l'état de l'invitation (true=invité/ false=invitation pas encore accepté)
	 * */
	public static HashMap<Integer, Chat> findByIDUser(int id, boolean b, boolean actual)
			throws SQLException, ClassNotFoundException, IOException {
		Connection cx = ConfigConnection.getUniqueInstance();
		Statement s = cx.createStatement();
		ResultSet res;
		/**
		 * Si le boolean recu est true, on veut donc obtenir les chats actuelles, cad les chats
		 * qui sont ne sont pas expirés, trouvés grâce à ">NOW()" (Mes chats)
		 * */
		if (actual == true)
			res = s.executeQuery("SELECT * FROM chat INNER JOIN correspondance ON "
					+ "chat.id_chat=correspondance.ID_chat WHERE correspondance.ID_cli=" + id
					+ " AND correspondance.invited=" + b +" AND ADDTIME(TIMESTAMP(date,horaire),duree)>NOW()");
			/**
			 * Si le boolean recu est false, on veut obtenir les chats expirés, trouvés grâce à "<=NOW()" (Historique de Mes Chats)
			 * */
		else
			res = s.executeQuery("SELECT * FROM chat INNER JOIN correspondance ON "
					+ "chat.id_chat=correspondance.ID_chat WHERE correspondance.ID_cli=" + id
					+ " AND correspondance.invited=" + b + " AND ADDTIME(TIMESTAMP(date,horaire),duree)<=NOW()");
		HashMap<Integer, Chat> all = new HashMap<Integer, Chat>();
		//On utilise le while pour parcourir tous les résultats de la requête et ajouter les chats dans la HashMap
		while (res.next()) {
			Chat d = new Chat(res.getString("titre"), res.getString("description"), res.getString("date"),
					res.getString("horaire"), res.getString("duree"), id);
			all.put(res.getInt("id_chat"), d);
		}
		return all;
	}

	/**
	 * Méthode qui retourne un HashMap contenant les Chats créés par l'utilisateur 
	 * */
	public static HashMap<Integer, Chat> findAllByIDUser(int id)
			throws SQLException, ClassNotFoundException, IOException {
		Connection cx = ConfigConnection.getUniqueInstance();
		Statement s = cx.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM chat INNER JOIN correspondance ON "
				+ "chat.id_chat=correspondance.ID_chat WHERE correspondance.ID_cli=" + id);
		HashMap<Integer, Chat> all = new HashMap<Integer, Chat>();
		while (res.next()) {
			Chat d = new Chat(res.getString("titre"), res.getString("description"), res.getString("date"),
					res.getString("horaire"), res.getString("duree"), id);
			all.put(res.getInt("id_chat"), d);
		}
		return all;
	}
	/**
	 * Méthode qui retourne un HashMap contenant tous les Chats de la base de données : 
	 * */
	public static HashMap<Integer, Chat> findAll(Connection cx) throws SQLException {
		Statement s = cx.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM chat");
		HashMap<Integer, Chat> all = new HashMap<Integer, Chat>();
		while (res.next()) {
			Chat d = new Chat(res.getString("titre"), res.getString("description"), res.getString("date"),
					res.getString("horaire"), res.getString("duree"), res.getInt("owner"));
			all.put(res.getInt("id_chat"), d);
		}
		return all;
	}
	
	public static void main(String [] args) throws ClassNotFoundException, SQLException, IOException {
		Calendar cal=GregorianCalendar.getInstance();
		cal.set(2021, 03, 11, 11, 30,0);
		Date d=cal.getTime();
		System.out.println(d);
		cal.set(2021, 03, 11, 23, 30);
		Date d2=cal.getTime();
		System.out.println(d.before(d2));
		System.out.println(GregorianCalendar.getInstance().getTime());
	}
}