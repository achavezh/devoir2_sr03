package Modele;

/*
 *  Cette classe est utilise par la fonction findUserToInvite() de la classe User. 
 *  Ceci permet  la fonction de renvoyer un tableau composé de UserInvited. 
 *  Le boolen du UserInvited permet alors de savoir si lutilisateur a été invité dans le chat 
 * */
public class UserInvited{
	public User user;
	public Boolean inChat;
	
	public UserInvited(){
	}
	
}
