package Controleur;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.server.ServerEndpointConfig;
import java.util.List;
import java.util.Map;

@ServerEndpoint(value = "/ChatEndPoint/{id_chat}/{login}", configurator = ChatEndPoint.EndpointConfigurator.class)
public class ChatEndPoint {

	// attribut unique singleton en static
	private static ChatEndPoint singleton = new ChatEndPoint();

	//  constructeur privé
	private ChatEndPoint() {
	}

	//fonction permettant de renvoyer le singleton
	public static ChatEndPoint getInstance() {
		return ChatEndPoint.singleton;
	}

	//Table contenant tous les chats actifs, pour chaque chat on a une liste d'utilisateurs connectés
	private static Hashtable<String, Hashtable<String, Session>> sessions = new Hashtable<>();
	
	//retourne la table sessions
	public static Hashtable<String, Hashtable<String, Session>> getHash() {
		return sessions;
	}

	//fonction se déclenchant à l'ouverture de la webSocket
	@OnOpen
	public void open(Session session, @PathParam("login") String pseudo, @PathParam("id_chat") String chat) {
		session.getUserProperties().put("login", pseudo);
		if (!sessions.containsKey(chat)) {//si le chat n'existe pas encore dans la table, on le rajoute
			//System.out.println("oui1");
			Hashtable<String, Session> data = new Hashtable<String, Session>();
			data.put(session.getId(), session);
			sessions.put(chat, data); // on ajoute le chat dans la table avec la table d'utilisateur connectée composé du nouvel utilisateur qui vient de se connecter
		} else {
			//System.out.println("oui2");
			Hashtable<String, Session> data = sessions.get(chat);// on récupère la table correspondant à la liste des utilisateurs connectés et on ajoute le nouvel utilisateur
			data.put(session.getId(), session);
		}
		sendMessage("True "+pseudo + " a rejoint la conversation", chat); // on envoie un message à tous prévenant d'une nouvelle connexion dans la salle, le boolean indique un message de connexion/déconnexion
	}

	//fonction se déclenchant à la fermeture de la webSocket
	@OnClose
	public void close(Session session, @PathParam("id_chat") String chat) {
		//on cherche l'utilisateur dans la liste des utilisateurs du chat et on le remove
		String pseudo = (String) session.getUserProperties().get("login");
		Hashtable<String, Session> data = sessions.get(chat);
		data.remove(session.getId()); 
		if (data.size() == 0) // si le chat est vide on le remove
			sessions.remove(chat);
		sendMessage("True "+pseudo + " a quitt la conversation", chat); // on envoie un message prévenant la deconnexion d'un utilisateur a tous les autres utilisateurs, le boolean indique un message de connexion/déconnexion
	}

	//fonction se déclenchant lors d'une erreur au niveau de la webSocket
	@OnError
	public void onError(Throwable error) {
		System.out.println("Error: " + error.getMessage());
	}

	//fonction se déclenchant à la récéption d'un message 
	@OnMessage
	public void handleMessage(String message, Session session, @PathParam("id_chat") String chat) {
		//on récupère le pseudo de l'expéditeur
		String pseudo = (String) session.getUserProperties().get("login");
		//on ajoute le pseudo au message et un boolean indiquant si le message est un message de connexion/déconnexion ou non
		String fullMessage ="False "+pseudo + " " + message;
		sendMessage(fullMessage, chat);// on appelle la fonction distribuant le message a tous les utilisateurs du chat
	}

	private void sendMessage(String fullMessage, String chat) {
		// Affichage sur la console du server Web.
		System.out.println(fullMessage);
		// On envoie le message Ã  tout le monde.
		Hashtable<String, Session> data = sessions.get(chat);
//		System.out.println("oui5");
		if (data != null) {
			for (Session session : data.values()) {
				try {
					//envoie du message à un utilisateur en particulier dans la webSocket
					session.getBasicRemote().sendText(fullMessage);
				} catch (Exception exception) {
					System.out.println("ERROR: cannot send message to " + session.getId());
				}
			}
		}
	}

	//fonction retournant la liste des utilisateurs connectés à un chat en particulier
	public static List<String> usersConnected(String chat) {
		List<String> u = new ArrayList<String>();
		Hashtable<String, Session> data = sessions.get(chat);
		if(data!=null) {
			for (Map.Entry<String, Session> s : data.entrySet()) {
				u.add((String) s.getValue().getUserProperties().get("login"));
			}
		}
		return u;
	}

	/**
	 * Permet de ne pas avoir une instance diffÃ©rente par client. ChatEndPoint est
	 * donc gÃ©rer en "singleton" et le configurateur utilise ce singleton.
	 */
	public static class EndpointConfigurator extends ServerEndpointConfig.Configurator {
		@Override
		@SuppressWarnings("unchecked")
		public <T> T getEndpointInstance(Class<T> endpointClass) {
			return (T) ChatEndPoint.getInstance();
		}
	}
}