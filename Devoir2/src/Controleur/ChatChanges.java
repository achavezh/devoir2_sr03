package Controleur;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Modele.Chat;
import Modele.ConfigConnection;
import Modele.User;

/**
 * Servlet implementation class ChatChanges
 */
@WebServlet("/ChatChanges")
public class ChatChanges extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChatChanges() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/**
		 * On peut recevoir 2 paramètres supplémentaires, orientant le traitement de la servlet par rapport aux chats :
		 * 1. Modifier
		 * 2. Supprimer
		 * **/
		
		/****1. MODIFICATION****/
		if (request.getParameter("modify") != null) {
			// On commence par récupérer les paramètres envoyés par l'utilisateur au moyen du formulaire :
			
			int id_chat = Integer.parseInt(request.getParameter("id_chat"));
			String titre = request.getParameter("titre");
			String description = request.getParameter("description");
			String date = request.getParameter("date");
			
			/*On utilise StringTokenizer pour couper le jour, le mois et l'année de la date, et on garde 
			*chaque information dans une variable :
			*/
			
			StringTokenizer S = new StringTokenizer(date, "-");
			String year = S.nextToken();
			String month = S.nextToken();
			String day = S.nextToken();
			
			//On fait un cast de la date recue 
			LocalDate d = LocalDate.of(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day));
			String horaire = request.getParameter("horaire");
			String duree = request.getParameter("duree");
			String owner = (String) request.getSession().getAttribute("login");
			owner.replace("'","\\'");
			
			//On va faire le même type de coupage pour l'heure recue et on fait aussi le cast
			StringTokenizer ST = new StringTokenizer(horaire, ":");
			String hour = ST.nextToken();
			String minutes = ST.nextToken();
			LocalTime sqlHoraire = LocalTime.of(Integer.parseInt(hour), Integer.parseInt(minutes));

			System.out.println(id_chat + "\n" + titre + "\n" + description + "\n" + date + "\n" + horaire + "\n" + duree
					+ "\n" + owner);
			
			/**
			* Nous vérifions si la date de début est anterieure à la date actuelle ou bien si
			*La date est égal à la date actuelle et l'heure est inférieure à l'heure actuelle
			*Si l'un de deux ou les deux sont vraies nous allons mettre un attribut pour envoyer au JSP
			*l'erreur
			**/
			if (d.isBefore(LocalDate.now()) || (d.isEqual(LocalDate.now()) && sqlHoraire.isBefore(LocalTime
					.of(LocalTime.now().getHour(), LocalTime.now().getMinute() - 1, LocalTime.now().getSecond())))) {
				RequestDispatcher rq=request.getRequestDispatcher("/Vue/ModifierChat.jsp");
				request.setAttribute("pastChosen", "");
				rq.forward(request, response); 

			} else {
			/**
			 * Sinon on va tout simplement chercher le chat dans la base de données
			 * Et gérer un objet de type Chat contruit par la base de données
			 * On va appeler ses méthodes set par chaque attribut de Chat
			 * Et finalement on utilise la méthode save pour faire un update à la bdd
			 * À la fin on va rédiriger au JSP qui contient les chats crées par l'utilisateur 
			 * */

				try {
					System.out.println(User.getIdUser(ConfigConnection.getUniqueInstance(), owner));
					Chat chatSupp = Chat.findByID(ConfigConnection.getUniqueInstance(), id_chat);
					chatSupp.setTitre(titre);
					chatSupp.setDescription(description);
					chatSupp.setDate(date);
					chatSupp.setHoraire(horaire);
					chatSupp.setDuree(duree);
					chatSupp.save(ConfigConnection.getUniqueInstance());
					response.sendRedirect("/Devoir2/Vue/Chats.jsp?actual");
				} catch (ClassNotFoundException | SQLException | IOException e) {
					e.printStackTrace();
				}
			}
		/****2. Suppresion****/
		} else if (request.getParameter("suppr") != null) {
			/**Pour la surpression on obtient le parametre qui contient l'id du chat 
			*On cherche le chat et encore une fois on créé un objet de type Chat
			*qui est contruit par la base de données.
			*Finalement on fait appel à la méthode delete pour faire la supression. 
			**/
			int id = Integer.parseInt(request.getParameter("id_chat"));
			try {
				Chat chatSupp = Chat.findByID(ConfigConnection.getUniqueInstance(), id);
				chatSupp.delete(ConfigConnection.getUniqueInstance());
				response.sendRedirect("/Devoir2/Vue/Chats.jsp?actual");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
