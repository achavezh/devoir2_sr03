package Controleur;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modele.*;

/**
 * Servlet implementation class Inviter
 */
@WebServlet("/GestionInvit")
public class GestionInvit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GestionInvit() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// cette servlet gère l'invitation d'un utilisateur mais aussi l'acceptation/refus d'une invitation
		if (request.getParameter("action").equals("invite")) {//si le motif d'appel est l'invitation d'utilisateurs
			List<String> u = null;
			if (request.getParameterValues("usersList") != null) //si la liste d'utilisateurs cochés (et donc à inviter) est non nulle
				u = Arrays.asList(request.getParameterValues("usersList"));// on créé une liste contenant les utilisateurs à inviter
			List<String> uVerif = Arrays.asList(request.getParameterValues("usersListVerif")); // liste de tous les utilisateurs que l'on peut inviter ou désinviter
			int id_chat = Integer.parseInt(request.getParameter("id_chat")); // on recupère le chat concerné par les invitations
			System.out.println(id_chat);
			int affected = 0;
			for (int i = 0; i < uVerif.size(); i++) {// on parcourt la liste de tous les utilisateurs que l'on peut inviter ou désinviter
				System.out.println(Integer.parseInt(uVerif.get(i)));
				CorrespClientChat invitation;
				try {
					if (u != null && u.contains(uVerif.get(i))) { // si l'utilisateur est dans la liste des utilisateurs que le propriétaire souhaite inviter
						invitation = CorrespClientChat.findByIDS(ConfigConnection.getUniqueInstance(),
								Integer.parseInt(uVerif.get(i)), id_chat);// on vérifie que l'invitation n'existe pas déjà dans le table
						if (invitation == null) { // si elle n'existe pas
							invitation = new CorrespClientChat(Integer.parseInt(uVerif.get(i)), id_chat, false);// on créé une nouvelle invitation
							affected += invitation.save(ConfigConnection.getUniqueInstance());//on save le tuple dans la table correspondance
							System.out.println("La1");
						}
						//si elle existe rien ne se produit bien sûr, car l'utilisateur est déjà invité
					} else {// si l'utilisateur n'est pas dans la liste des utilisateurs à inviter
						System.out.println("La3");
						invitation = CorrespClientChat.findByIDS(ConfigConnection.getUniqueInstance(),
								Integer.parseInt(uVerif.get(i)), id_chat); // ici on doit désinviter l'utilisateur donc on récupère le tuple correspondant
						if (invitation != null) {// si on trouve le tuple
							affected += invitation.delete(ConfigConnection.getUniqueInstance());// on supprime le tuple de la table correspondance pour désinviter l'utilisateur
							System.out.println("La2");
						}
					}

				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (affected > 0) //si des modifications ont été effectuées (modification ou suppression) on redirige avec un paramètre indiquant un message de réussite sur le JSP
				response.sendRedirect("/Devoir2/Vue/InviterUser.jsp?goodSelect&id_chat=" + id_chat);
			else
				response.sendRedirect("/Devoir2/Vue/InviterUser.jsp?id_chat=" + id_chat);
			/*
			 * RequestDispatcher rd = request.getRequestDispatcher("/Vue/InviterUser.jsp");
			 * rd.include(request, response);
			 */

		} else {
			if (request.getParameter("action").equals("accepter")) { // si la servlet a été appelée pour accepter une invitation
				try {
					CorrespClientChat maj = CorrespClientChat.findByIDS(ConfigConnection.getUniqueInstance(),
							Integer.parseInt(request.getParameter("id_cli")),
							Integer.parseInt(request.getParameter("id_chat"))); //on récupère la ligne de l'invitation
					maj.setInvited(true);// on met à true le boolean pour indiquer que l'invitation a été acceptée
					maj.save(ConfigConnection.getUniqueInstance()); // on sauvegarde les modifications dans la BDD
					response.sendRedirect("/Devoir2/Vue/Invitations.jsp?actual");// on redirige vers la page permettant de visualiser les invitations
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				if (request.getParameter("action").equals("refuser")) { // si la servlet a été appelée pour supprimer une invitation
					try {
						CorrespClientChat maj = CorrespClientChat.findByIDS(ConfigConnection.getUniqueInstance(),
								Integer.parseInt(request.getParameter("id_cli")),
								Integer.parseInt(request.getParameter("id_chat")));//on récupère la ligne de l'invitation
						maj.delete(ConfigConnection.getUniqueInstance());// on delete l'invitation correspondante
						response.sendRedirect("/Devoir2/Vue/Invitations.jsp?actual");// on redirige vers la page permettant de voir les invitations
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
			}
		}

	}

}
