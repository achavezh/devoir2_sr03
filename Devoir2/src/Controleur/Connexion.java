package Controleur;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Modele.User;
import Modele.ConfigConnection;

/**
 * Servlet implementation class Connexion
 */
@WebServlet("/Connexion")
public class Connexion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Connexion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("/Devoir2/Vue/Connexion.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/**
		 * La méthode pour la connexion est similaire à celle utilisée dans le TD3
		 * 1. On obtient les paramtres mail et mot de passe
		 * 	a. On valide que le user existe avec son mail et mot de passee et on initialise la session
		 * en mettant comme ses attributs, les paramtres recus. 
		 * 	b. Si l'user n'existe pas ou les paramtres ne sont pas corrects on redirige en mettant
		 * comme attributs les erreurs respectifs. 
		 * */
		
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		try {
			if(User.findByLoginAndPwd(ConfigConnection.getUniqueInstance(), email, password)){
				HttpSession session = request.getSession();
			    session.setAttribute("login", request.getParameter("email"));
			    session.setAttribute("password", request.getParameter("password"));
			    response.setContentType("text/html;charset=UTF-8");
			    response.sendRedirect("/Devoir2/Vue/Accueil.jsp");    	 
			}else {
				RequestDispatcher rq=request.getRequestDispatcher("/Vue/Connexion.jsp");
				request.setAttribute("badValue", "");
				request.setAttribute("tryRegistered", "");
				rq.forward(request, response);  
			}
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
		
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

}
