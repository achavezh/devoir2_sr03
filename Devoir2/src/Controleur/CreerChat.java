package Controleur;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modele.*;

/**
 * Servlet implementation class CreerChat
 */
@WebServlet("/CreerChat")
public class CreerChat extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CreerChat() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/** Cette servlet est dédiée à la création d'un chat
		 * 1. On commencne par obtenir les paramètres du formulaire de création d'un chat 
		 * 2. On coupe la date avec StringTokenizer et on fait le cast
		 * 3. On coupe l'heure et on fait le cast
		 * 4. On vérifie que le chat n'est pas défini dans le passé
		 * */
		String titre = request.getParameter("titre");
		String description = request.getParameter("description");
		String date = request.getParameter("date");
		StringTokenizer S = new StringTokenizer(date, "-");
		String year = S.nextToken();
		String month = S.nextToken();
		String day = S.nextToken();
		LocalDate d = LocalDate.of(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day));
		String horaire = request.getParameter("horaire");
		String duree = request.getParameter("duree");
		String owner = (String) request.getSession().getAttribute("login");
		StringTokenizer ST = new StringTokenizer(horaire, ":");
		String hour = ST.nextToken();
		String minutes = ST.nextToken();
		LocalTime sqlHoraire = LocalTime.of(Integer.parseInt(hour), Integer.parseInt(minutes));
		/*CAS 1 : Date inferieure à la date actuelle ou 
		 * date égale à la date actuelle mais horaire inférieur à l'heure actuelle  
		 * Alors on redirige le JSP avec une erreur*/
		if (d.isBefore(LocalDate.now()) || (d.isEqual(LocalDate.now()) && sqlHoraire.isBefore(LocalTime
				.of(LocalTime.now().getHour(), LocalTime.now().getMinute() - 1, LocalTime.now().getSecond())))) {
			RequestDispatcher rq=request.getRequestDispatcher("/Vue/CreerChat.jsp");
			request.setAttribute("pastChosen", "");
			rq.forward(request, response); 
		} else {
		/*CAS 2 : Date et heure valides, nous allons créer un objet de type chat avec le constructeur qui reçoit
		 * les valeurs des paramètres
		 * Finalement nous allons créer le tuple dans la BDD avec la méthode save et on redirige directement au JSP*/
			try {
				System.out.println(User.getIdUser(ConfigConnection.getUniqueInstance(), owner));
				Chat c = new Chat(titre, description, date, horaire, duree,
						User.getIdUser(ConfigConnection.getUniqueInstance(), owner));
				c.save(ConfigConnection.getUniqueInstance());
				response.sendRedirect("/Devoir2/Vue/CreerChat.jsp?created");
			} catch (ClassNotFoundException | SQLException | IOException e) {
				e.printStackTrace();
			}
		}
	}

}
