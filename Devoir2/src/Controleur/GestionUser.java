package Controleur;

import java.io.IOException;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modele.User;
import Modele.ConfigConnection;

/**
 * Servlet implementation class AjouterUser
 */
@WebServlet("/GestionUser")
public class GestionUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GestionUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//on récupère les informations du formulaire de création d'un utilisateur ou  de modification des informations d'un utilisateur
		String firstName = request.getParameter("firstName");
		String famillyName = request.getParameter("famillyName");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String gender = request.getParameter("gender");
		boolean s = true;
		try {
			if (User.userExistante(ConfigConnection.getUniqueInstance(), email)) { // si l'utilisateur existe déjà (même mail=login)
				if (request.getParameter("create") != null) { // si le formulaire était celui de création, on renvoie la requête avec une erreur
					RequestDispatcher rq=request.getRequestDispatcher("/Vue/AjouterUser.jsp");
					request.setAttribute("alreadyExist", "");
					rq.forward(request, response);
					s=false;
				} else {
					if (!request.getSession().getAttribute("login").toString().equals(request.getParameter("email"))) { // sinon, si le formulaire est celui de modification, et que le nouveau login n'est pas égale à l'ancien (des changements ont été effectués) on renvoie la requête vers ls JSP de modification
						RequestDispatcher rq=request.getRequestDispatcher("/Vue/ChangeUser.jsp");
						rq.forward(request, response);
						s=false;
					}
				}
			}
			if (s == true) { // si l'utilisateur n'existe pas
				if (request.getParameter("create") != null) {// si le formulaire était un formulaire de création
					User u = new User(firstName, famillyName, email, password, gender);
					u.save(ConfigConnection.getUniqueInstance()); // on sauvegarde le nouvel utilisateur dans la base de données
					RequestDispatcher rq=request.getRequestDispatcher("/Vue/Connexion.jsp"); // on redirige vers le JSP de connexion
					request.setAttribute("tryRegistered","");// avec un paramètre qui va permettre l'affichage du mail choisi pendant la création
					rq.forward(request, response);
				} else {// si le formulaire était un formulaire de modification
					HttpSession session = request.getSession();
					User u = User.findByLogin(ConfigConnection.getUniqueInstance(),
							session.getAttribute("login").toString()); // on retrouve le tuple de la base et on en créé un objet
					//on modifie ses attributs
					u.setFaName(famillyName);
					u.setFiName(firstName);
					u.setGender(gender);
					u.setPassword(password);
					u.setMail(email); 
					//on change le login de session
					session.setAttribute("login", email);
					// on sauvegarde la mise à jour dans la base de données
					u.save(ConfigConnection.getUniqueInstance());
					RequestDispatcher rq=request.getRequestDispatcher("/Vue/ChangeUser.jsp");
					request.setAttribute("changes","");// on renvoie vers le JSP de modification des infos utilisateurs aavec un paramètre permettant l'affichage d'un message validation des modifications
					rq.forward(request, response);
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
