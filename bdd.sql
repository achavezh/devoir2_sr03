-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: sr03
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chat` (
  `id_chat` int NOT NULL AUTO_INCREMENT,
  `owner` int NOT NULL,
  `titre` varchar(200) NOT NULL,
  `description` text,
  `date` date NOT NULL,
  `horaire` time NOT NULL,
  `duree` time NOT NULL,
  PRIMARY KEY (`id_chat`),
  KEY `owner` (`owner`),
  CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`owner`) REFERENCES `user` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `correspondance`
--

DROP TABLE IF EXISTS `correspondance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `correspondance` (
  `ID_chat` int NOT NULL,
  `ID_cli` int NOT NULL,
  `invited` bit(1) NOT NULL,
  PRIMARY KEY (`ID_cli`,`ID_chat`),
  KEY `ID_chat` (`ID_chat`),
  CONSTRAINT `correspondance_ibfk_1` FOREIGN KEY (`ID_cli`) REFERENCES `user` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `correspondance_ibfk_2` FOREIGN KEY (`ID_chat`) REFERENCES `chat` (`id_chat`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(100) NOT NULL,
  `FamilyName` varchar(100) NOT NULL,
  `Mail` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Genre` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `usersinvited`
--

DROP TABLE IF EXISTS `usersinvited`;
/*!50001 DROP VIEW IF EXISTS `usersinvited`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `usersinvited` AS SELECT 
 1 AS `ID`,
 1 AS `FirstName`,
 1 AS `FamilyName`,
 1 AS `Mail`,
 1 AS `Password`,
 1 AS `Genre`,
 1 AS `ID_cli`,
 1 AS `ID_chat`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `usersinvited`
--

/*!50001 DROP VIEW IF EXISTS `usersinvited`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `usersinvited` AS select `user`.`ID` AS `ID`,`user`.`FirstName` AS `FirstName`,`user`.`FamilyName` AS `FamilyName`,`user`.`Mail` AS `Mail`,`user`.`Password` AS `Password`,`user`.`Genre` AS `Genre`,`correspondance`.`ID_cli` AS `ID_cli`,`correspondance`.`ID_chat` AS `ID_chat` from (`user` left join `correspondance` on((`user`.`ID` = `correspondance`.`ID_cli`))) union select `user`.`ID` AS `ID`,`user`.`FirstName` AS `FirstName`,`user`.`FamilyName` AS `FamilyName`,`user`.`Mail` AS `Mail`,`user`.`Password` AS `Password`,`user`.`Genre` AS `Genre`,`correspondance`.`ID_cli` AS `ID_cli`,`correspondance`.`ID_chat` AS `ID_chat` from (`correspondance` left join `user` on((`user`.`ID` = `correspondance`.`ID_cli`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-04 16:50:36
